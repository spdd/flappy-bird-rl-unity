﻿using UnityEngine;
using System.Collections;

public class Pipe : MonoBehaviour {

	// Minor class used to tag parent GameObject

	public Transform pipeDown = null;
	public Transform scoreTrigger = null;
	public Transform pipeUp = null;

	public Vector3 Position {
		get {
			return new Vector3(
				pipeUp.position.x + pipeUp.renderer.bounds.size.x,
				pipeUp.position.y,
				pipeUp.position.z
			);
		}
	}

}
