﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ContinuousSpawner : MonoBehaviour {

	// NOTE For efficiency, we could use a pool mechanism and disable, reposition and
	// reorient objects accordingly to avoid instantiating-destroying objects continously.
	// For our purposes, this is not necessary since there are no major performance issues.

	public GameObject spawnObject = null;

	public GameObject parentGameObject = null;
	public GameObject lastSpawnedObject = null;

	public bool isSpawning = true;

	// Update is called once per frame
	void Update() {
		if (isSpawning && (lastSpawnedObject == null || ShouldSpawn)) {
			GameObject newObject = Spawn();
			newObject.transform.position = SpawnPosition;

			lastSpawnedObject = newObject;
		}
	}

	protected virtual bool ShouldSpawn {
		get {
			return (lastSpawnedObject.transform.position.x + spawnObject.renderer.bounds.size.x) < gameObject.transform.position.x;
		}
	}

	protected virtual Vector3 SpawnPosition {
		get {
			return (lastSpawnedObject == null ?
				 gameObject.transform.position :
				 new Vector3(
					lastSpawnedObject.transform.position.x + spawnObject.renderer.bounds.size.x,
					lastSpawnedObject.transform.position.y,
					lastSpawnedObject.transform.position.z
				)
			);
		}
	}

	protected virtual GameObject Spawn() {
		GameObject newObject = GameObject.Instantiate(spawnObject) as GameObject;
		newObject.name = spawnObject.name;
		newObject.transform.parent = parentGameObject.transform;

		return newObject;
	}

}
