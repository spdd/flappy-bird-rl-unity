﻿using UnityEngine;
using System.Collections;

using System;
using System.IO;

public class GameController : MonoBehaviour {

	private static object _lock = new object();
	private static GameController _instance = null;

	public string statisticsFile = @"/FlappyBirdRL.csv";
	public float restartDelay = 1.0f;
	public bool sound = true;

	public int bestScore = 0;

	private int runCount = 0;
	private DateTime startTime = DateTime.Now;

	#region Game References

	private Scroller scroller = null;
	private ScoreController scoreController = null;
	private PlayerCharacter player = null;

	private PlayerCharacter.Memento playerMemento = null;

	#endregion

	// Reference: http://wiki.unity3d.com/index.php/Singleton
	// Reference: http://answers.unity3d.com/questions/20773/how-do-i-make-a-highscores-board.html
	public static GameController Instance {
		get {
			if (_instance == null) {
				MakeSingleton(new GameObject("GameController"));
			}

			return _instance;
		}
	}

	private static GameObject MakeSingleton(GameObject container) {
		lock(_lock) {
			if (_instance == null) {
				_instance = container.GetComponent<GameController>();
				
				if (_instance == null) {
					_instance = container.AddComponent<GameController>();
				}

				DontDestroyOnLoad(container);
				
				_instance.Initialise();
			}
		}

		return container;
	}
	
	private void Initialise() {
		try {
			string statisticsPath = Application.persistentDataPath + statisticsFile;

			// Overwrite file on each run
			Statistics = new StatisticsFile(File.Create(statisticsPath));
			Debug.Log("Writing statistics to [" + statisticsPath + "]");

			Statistics.WriteHeader();
		} catch (Exception ex) {
			Debug.LogException(ex);
			Statistics = new StatisticsFile(null);
		}
	}

	public StatisticsFile Statistics { get; private set; }

	public bool GameOver { get; private set; }
	
	public int RunCount {
		get {
			return runCount;
		}
	}

	public void TriggerGameOver() {
		if (!GameOver) {
			GameOver = true;

			if (scroller != null) {
				scroller.isScrolling = false;
			}

			if (scoreController != null) {
				bestScore = Math.Max(bestScore, scoreController.Score);
			}

			Invoke("Restart", restartDelay);
		}
	}

	private void Restart() {
		Statistics.Write(CreateRecordEntry());
		
		// Keep a record of important references which need to be restored later
		if (player != null) {
			playerMemento = player.State;
		}

		++runCount;
		Application.LoadLevel(Application.loadedLevel);
	}

	private StatisticsFile.Record CreateRecordEntry() {
		StatisticsFile.Record record = new StatisticsFile.Record();
		
		record.ID = runCount;

		if (player != null) {
			record.LearningRate = player.intelligenceProperties.qLearning.learningRate;
			record.DiscountRate = player.intelligenceProperties.qLearning.discountRate;
			record.ExplorationRate = player.intelligenceProperties.qLearning.explorationRate;
			record.SpacePrecision = player.intelligenceProperties.space.precision;
			record.AliveReward = player.intelligenceProperties.rewards.aliveReward;
			record.DeathReward = player.intelligenceProperties.rewards.deathReward;
		}

		record.StartTime = startTime;
		record.Duration = DateTime.Now - startTime;
		record.Score = GetScore();

		// Disabling tag to conserve file size since it can get huge pretty quickly...  
		//record.Tag = GetPlayerState();

		return record;
	}
	
	private int GetScore() {
		return (scoreController == null ? -1 : scoreController.Score);
	}

	private string GetPlayerState() {
		try {
			if (player != null &&
			    (player.controllerType == PlayerCharacter.ControllerType.AIController ||
			 		player.controllerType == PlayerCharacter.ControllerType.HybridController)) {
				return Utilities.Base64Serialize(player.Controller);
			}
		} catch (Exception ex) {
			Debug.LogException(ex);
		}

		return "";
	}

	// Use this for initialization
	void Start () {
		if (_instance == null) {
			MakeSingleton(gameObject);
			OnStart();
		} else if (_instance != this) {
			Destroy(gameObject);
		}
	}
	
	// Since Start() will only be called once since this object is
	// retained between scene loads, we need to use OnLevelWasLoaded
	// to identify scene loads.
	//
	// OnLevelWasLoaded is initialized after all important GameObjects
	// have their 'Awake' event triggered.
	void OnLevelWasLoaded(int level) {
		OnStart();
	}

	private void OnStart() {
		startTime = DateTime.Now;
		GameOver = false;

		// Find necessary GameObjects

		// We require to use Find here since this singleton instance will persist amongst scene loads
		// which implies that the ScoreController instance and Scroller instance will be constructed
		// anew each time.
		scoreController = GameObject.FindObjectOfType(typeof(ScoreController)) as ScoreController;
		scroller = GameObject.FindObjectOfType(typeof(Scroller)) as Scroller;
		player = GameObject.FindObjectOfType(typeof(PlayerCharacter)) as PlayerCharacter;
		
		// Restore important references
		if (player != null && playerMemento != null) {
			player.State = playerMemento;
		}
	}

	void OnApplicationQuit() {
		Statistics.Write(CreateRecordEntry());

		// Persist data
		Statistics.Close();
	}
}