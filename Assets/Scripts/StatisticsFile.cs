﻿using UnityEngine;

using System;
using System.IO;

using System.Collections;
using System.Collections.Generic;

public class StatisticsFile
{
	public class Record {
		public int ID { get; set; }
		public float LearningRate { get; set; }
		public float DiscountRate { get; set; }
		public float ExplorationRate { get; set; }
		public Vector2 SpacePrecision { get; set; }
		public float AliveReward { get; set; }
		public float DeathReward { get; set; }
		public DateTime StartTime { get; set; }
		public TimeSpan Duration { get; set; }
		public int Score { get; set; }
		public string Tag { get; set; }
	}

	private StreamWriter _file = null;

	public StatisticsFile(FileStream file) {
		_file = (file == null ? null : new StreamWriter(new BufferedStream(file)));
	}

	public void WriteHeader() {
		if (_file != null) {
			_file.WriteLine(Header);
		}
	}

	public void Write(Record record) {
		if (_file != null) {
			_file.WriteLine(Format(record));
		}
	}

	public void Write(List<Record> records) {
		if (_file != null) {
			foreach (Record record in records) {
				Write(record);
			}
		}
	}

	public void Close() {
		if (_file != null) {
			_file.Close();
		}
	}

	private string Header {
		get {
			// NOTE ID field is in lowercase due to the following...
			// Reference: http://support.microsoft.com/kb/323626
			return "id,learning rate,exploration rate,discount rate,precision,alive reward,death reward,start time,duration,final score,tag";
		}
	}

	private string Format(Record record) {
		return record.ID + "," +
			record.LearningRate + "," +
			record.ExplorationRate + "," +
			record.DiscountRate + "," +
			"\"" + record.SpacePrecision.ToString() + "\"," +
			record.AliveReward + "," +
			record.DeathReward + "," +
			record.StartTime.ToString() + "," +
			record.Duration.TotalSeconds + "," +
			record.Score + "," +
			record.Tag;
	}
}