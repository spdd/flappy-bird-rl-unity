﻿using UnityEngine;
using System.Collections;

public class RandomSprite : MonoBehaviour {

	public Sprite[] sprites = new Sprite[0];

	// Use this for initialization
	void Start () {
	
		SpriteRenderer renderer = GetComponent<SpriteRenderer>();

		if (renderer != null && sprites.Length > 0) {
			renderer.sprite = sprites[Random.Range(0, sprites.Length)];
		}

	}

}
