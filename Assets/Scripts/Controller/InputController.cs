//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using UnityEngine;
using System;

public class InputController : Controller
{
	public InputController(PlayerCharacter player) {
		this.Player = player;
	}

	public PlayerCharacter Player { get; set; }

	public void Update() {

		if (!Player.Alive) {
			GameController.Instance.TriggerGameOver();
			return;
		}

		if (Input.GetButtonDown("Jump")) {
			Player.Jump();
		}

	}
}