﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

	public string[] destroyObjectsWithTag = new string[0];

	void OnTriggerEnter2D(Collider2D other) {
		Transform toDestroy = null;

		// Reference: http://unity3d.com/learn/tutorials/modules/beginner/live-training-archive/infinite-runner
		// Alternative Implementation: http://unity3d.com/learn/tutorials/projects/space-shooter/boundary
		foreach (string tag in destroyObjectsWithTag) {
			toDestroy = Utilities.GetTransformWithTagInHierarchy(other.transform, tag);

			if (toDestroy != null) {
				Destroy(toDestroy.gameObject);
			}
		}
	}
}
