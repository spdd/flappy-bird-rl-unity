﻿using UnityEngine;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class PlayerCharacter : MonoBehaviour {

	[System.Serializable]
	public class AIProperties {
		public string savedState = null;
		public AI.QLearningProperties qLearning = new AI.QLearningProperties();

		public AIController.RewardProperties rewards = new AIController.RewardProperties();
		public AIController.SpaceQuantizationProperties space = new AIController.SpaceQuantizationProperties();
	}

	// The bird's velocity upon jumping 
	public float flapResultantVelocity = 3.0f;

	// Maintain a list of spawned pipes, available to
	// controllers via the GetPipes(int) method
	public PipeSpawner pipeSpawner = null;
	private List<GameObject> pipes = new List<GameObject>();

	// Bird status
	private bool alive = true;

	// Animation component reference
	private Animator animator = null;
	
	// Underlying controller type
	public enum ControllerType { InputController, AIController, HybridController };
	public ControllerType controllerType = ControllerType.AIController;
	
	// Underlying controller strategy instance
	private Controller controller = null;

	public AIProperties intelligenceProperties = new AIProperties();

	void Awake() {
		animator = gameObject.GetComponent<Animator>();
		animator.SetInteger("Colour", UnityEngine.Random.Range(0, 4));

		switch (controllerType) {
		case ControllerType.HybridController: {
			Controller = InitialiseAIController(new HybridController(this));
			break;
		}
		case ControllerType.AIController: {
			Controller = InitialiseAIController(new AIController(this));
			break;
		}
		case ControllerType.InputController: {
			Controller = new InputController(this);
			break;
		}
		default: {
			Controller = new NullController();
			break;
		}
		}
	}

	private AIController InitialiseAIController(AIController ai) {
		// Load Saved QLearning State if available
		if (!string.IsNullOrEmpty(intelligenceProperties.savedState )) {

			try {
				AI.QLearning<QLearnState, QLearnAction> algorithm = Utilities.Base64Deserialize<AI.QLearning<QLearnState, QLearnAction> >(intelligenceProperties.savedState);
				if (algorithm != null) {
					ai.QLearningAlgorithm = algorithm;
				}
			} catch (System.Exception ex) {
				Debug.LogException(ex);
			}

		}

		// Set the properties reference to the instance contained herein so that
		// modifications in the 'Game' view will be made available to the algorithm
		ai.QLearningAlgorithm.Properties = intelligenceProperties.qLearning;
		ai.Rewards = intelligenceProperties.rewards;
		ai.SpaceQuantization = intelligenceProperties.space;

		return ai;
	}

	// Use this for initialization
	void Start() {

	}
	
	// Update is called once per frame
	void Update() {
		// Record upcoming pipes
		UpdatePipes();

		// Delegate update to controller implementation
		controller.Update();
	}

	private void UpdatePipes() {

		if (pipeSpawner != null) {
			// Remove pipes which the player has traversed
			// NOTE No need for loop since we are assuming that the player
			//      can only traverse one pipe at a time
			//      (i.e. no overlapping pipes)
			if (pipes.Count > 0) {
				// Check the first Pipe GameObject in the list 
				Pipe pipe = pipes[0].GetComponent<Pipe>();
				// Remove pipe if player has surpassed it
				if (pipe != null && transform.position.x > pipe.Position.x) {
					pipes.RemoveAt(0);
				}
			}
			
			// Add new pipes
			if (pipes.LastOrDefault() != pipeSpawner.lastSpawnedObject) {
				pipes.Add(pipeSpawner.lastSpawnedObject);
			}
		}

	}

	public Controller Controller {
		get {
			return controller;
		}

		set {
			controller = value;
			controller.Player = this;
		}
	}

	public bool Alive {
		get {
			return alive;
		}
	}

	public List<GameObject> GetPipes(int count) {
		return pipes.GetRange(0, Mathf.Min(count, pipes.Count));
	}

	public void Jump() {
		// Reference: http://www.geek.com/games/how-flappy-birds-physics-are-cheating-you-1584264/
		// Note: The underlying implementation can cause some issues since it is dependant on frame rate
		//Vector2 flapForce = rigidbody2D.mass / Time.deltaTime * new Vector2(0.0f, (flapResultantVelocity - rigidbody2D.velocity.y));
		//rigidbody2D.AddForce(flapForce);

		rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, flapResultantVelocity);
	}

	public void Reset() {
		alive = true;
		animator.enabled = true;
	}

	public void Kill() {
		alive = false;

		// Play 'Hit' sound
		if (audio != null && GameController.Instance.sound) {
			audio.Play();
		}

		// Stop animation by simply disabling the whole component
		if (animator != null) {
			animator.enabled = false;
		}
	}

	public class Memento {
		private float flapResultantVelocity = 3.0f;
		private ControllerType controllerType = ControllerType.InputController;
		private Controller controller = null;
		private AIProperties intelligenceProperties = null;

		internal Memento(PlayerCharacter character) {
			flapResultantVelocity = character.flapResultantVelocity;
			controllerType = character.controllerType;
			controller = character.controller;
			intelligenceProperties = character.intelligenceProperties;
		}

		internal void Restore(PlayerCharacter character) {
			character.flapResultantVelocity = flapResultantVelocity;
			character.controllerType = controllerType;
			character.intelligenceProperties = intelligenceProperties;

			// Use property to ensure that Player reference in controller is set accordingly
			character.Controller = controller;

			if (character.controllerType == ControllerType.AIController ||
			    character.controllerType == ControllerType.HybridController) {
				AIController aiController = ((AIController) character.Controller);
				
				// Link back the references so that changes in the 'Game' view
				// are visible to the controller instance!
				aiController.Rewards = character.intelligenceProperties.rewards;
				aiController.SpaceQuantization = character.intelligenceProperties.space;
				aiController.QLearningAlgorithm.Properties = character.intelligenceProperties.qLearning;
			}
		}
	}

	public Memento State {
		get {
			return new Memento(this);
		}

		set {
			value.Restore(this);
		}
	}

}
